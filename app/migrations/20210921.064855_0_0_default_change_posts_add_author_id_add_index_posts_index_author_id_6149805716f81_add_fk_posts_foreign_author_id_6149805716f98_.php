<?php

namespace Migration;

use Spiral\Migrations\Migration;

class OrmDefaultBd8d4f8c18784a8cc1ca9349101f89a9 extends Migration
{
    protected const DATABASE = 'default';

    public function up(): void
    {
        $this->table('posts')
            ->addColumn('author_id', 'integer', [
                'nullable' => false,
                'default'  => null
            ])
            ->addIndex(["author_id"], [
                'name'   => 'posts_index_author_id_6149805716f81',
                'unique' => false
            ])
            ->addForeignKey(["author_id"], 'users', ["id"], [
                'name'   => 'posts_foreign_author_id_6149805716f98',
                'delete' => 'CASCADE',
                'update' => 'CASCADE'
            ])
            ->update();
        
        $this->table('comments')
            ->addColumn('post_id', 'integer', [
                'nullable' => false,
                'default'  => null
            ])
            ->addColumn('author_id', 'integer', [
                'nullable' => false,
                'default'  => null
            ])
            ->addIndex(["post_id"], [
                'name'   => 'comments_index_post_id_6149805717660',
                'unique' => false
            ])
            ->addIndex(["author_id"], [
                'name'   => 'comments_index_author_id_61498057176a2',
                'unique' => false
            ])
            ->addForeignKey(["post_id"], 'posts', ["id"], [
                'name'   => 'comments_foreign_post_id_614980571766f',
                'delete' => 'CASCADE',
                'update' => 'CASCADE'
            ])
            ->addForeignKey(["author_id"], 'users', ["id"], [
                'name'   => 'comments_foreign_author_id_61498057176aa',
                'delete' => 'CASCADE',
                'update' => 'CASCADE'
            ])
            ->update();
    }

    public function down(): void
    {
        $this->table('comments')
            ->dropForeignKey(["post_id"])
            ->dropForeignKey(["author_id"])
            ->dropIndex(["post_id"])
            ->dropIndex(["author_id"])
            ->dropColumn('post_id')
            ->dropColumn('author_id')
            ->update();
        
        $this->table('posts')
            ->dropForeignKey(["author_id"])
            ->dropIndex(["author_id"])
            ->dropColumn('author_id')
            ->update();
    }
}
