<?php

declare(strict_types=1);

return [
    'procedures' => [
        'apiTest' => App\Procedure\ApiTest::class,
        'adminTest' => App\Procedure\AdminTest::class,
    ],
];
