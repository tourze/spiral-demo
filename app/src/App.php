<?php

/**
 * This file is part of Spiral package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App;

use App\Bootloader;
use App\Bootloader\ExceptionCatchBootloader;
use Spiral\Bootloader as Framework;
use Spiral\Bootloader\AttributesBootloader;
use Spiral\Bootloader\Broadcast\BroadcastBootloader;
use Spiral\Bootloader\Http\WebsocketsBootloader;
use Spiral\Bootloader\Storage\StorageBootloader;
use Spiral\DataGrid\Bootloader\GridBootloader;
use Spiral\DotEnv\Bootloader as DotEnv;
use Spiral\Framework\Kernel;
use Spiral\Monolog\Bootloader as Monolog;
use Spiral\Nyholm\Bootloader as Nyholm;
use Spiral\Prototype\Bootloader as Prototype;
use Spiral\Router\Bootloader\AnnotatedRoutesBootloader;
use Spiral\Scaffolder\Bootloader as Scaffolder;
use Spiral\Sentry\Bootloader\SentryBootloader;
use Spiral\Stempler\Bootloader as Stempler;

class App extends Kernel
{
    /*
     * List of components and extensions to be automatically registered
     * within system container on application start.
     */
    protected const LOAD = [
        // Base extensions
        DotEnv\DotenvBootloader::class,
        Monolog\MonologBootloader::class,
        AttributesBootloader::class,
        StorageBootloader::class, // TODO 参考 https://spiral.dev/docs/component-storage#framework-integration 配置七牛上传
        GridBootloader::class, // TODO 参考 https://spiral.dev/docs/component-data-grid
        BroadcastBootloader::class, // TODO 参考 https://spiral.dev/docs/broadcast-configuration
        WebsocketsBootloader::class, // TODO 参考 https://spiral.dev/docs/broadcast-websockets
        SentryBootloader::class, // TODO 参考 https://spiral.dev/docs/extension-sentry

        // Application specific logs
        Bootloader\LoggingBootloader::class,

        // Core Services
        Framework\SnapshotsBootloader::class,
        Framework\I18nBootloader::class,

        // Security and validation
        Framework\Security\EncrypterBootloader::class,
        Framework\Security\ValidationBootloader::class,
        Framework\Security\FiltersBootloader::class,
        Framework\Security\GuardBootloader::class,

        // HTTP extensions
        Nyholm\NyholmBootloader::class,
        Framework\Http\RouterBootloader::class,
        // 拦截错误
        ExceptionCatchBootloader::class,
        //Framework\Http\ErrorHandlerBootloader::class,
        Framework\Http\JsonPayloadsBootloader::class,
        Framework\Http\CookiesBootloader::class,
        Framework\Http\SessionBootloader::class,
        Framework\Http\CsrfBootloader::class,
        Framework\Http\PaginationBootloader::class,
        // https://spiral.dev/docs/http-annotated-routes 引入注解路由的支持
        AnnotatedRoutesBootloader::class,

        // Databases
        Framework\Database\DatabaseBootloader::class,
        Framework\Database\MigrationsBootloader::class,

        // ORM
        Framework\Cycle\CycleBootloader::class,
        Framework\Cycle\ProxiesBootloader::class,
        Framework\Cycle\AnnotatedBootloader::class,

        // Views and view translation
        Framework\Views\ViewsBootloader::class,
        Framework\Views\TranslatedCacheBootloader::class,

        // Additional dispatchers
        Framework\Jobs\JobsBootloader::class,

        // Extensions and bridges
        Stempler\StemplerBootloader::class,

        // Framework commands
        Framework\CommandBootloader::class,
        Scaffolder\ScaffolderBootloader::class,

        // Debug and debug extensions
        Framework\DebugBootloader::class,
        Framework\Debug\LogCollectorBootloader::class,
        Framework\Debug\HttpCollectorBootloader::class,
    ];

    /*
     * Application specific services and extensions.
     */
    protected const APP = [
        Bootloader\LocaleSelectorBootloader::class,
        Bootloader\RoutesBootloader::class,

        // fast code prototyping
        Prototype\PrototypeBootloader::class,

        // 假数据
        Bootloader\FakerBootloader::class,

        // 参考文档 http://www.studyspiral.cn/docs/basics/quick-start/#%E9%A2%86%E5%9F%9F%E5%86%85%E6%A0%B8 加的
        Bootloader\AppBootloader::class,
    ];
}
