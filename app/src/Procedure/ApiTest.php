<?php

namespace App\Procedure;

use App\JsonRPC\Procedure;

class ApiTest extends Procedure
{
    public int $id;

    public function execute(): array
    {
        return [
            'time' => time(),
            'id' => $this->id,
        ];
    }
}
