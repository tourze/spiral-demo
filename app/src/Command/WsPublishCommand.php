<?php

namespace App\Command;

use Spiral\Broadcast\BroadcastInterface;
use Spiral\Broadcast\Message;
use Spiral\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class WsPublishCommand extends Command
{
    protected const NAME = 'ws-publish';

    protected const DESCRIPTION = '测试发布ws消息';

    protected const ARGUMENTS = [
        ['message', InputArgument::REQUIRED]
    ];

    public function perform(BroadcastInterface $broadcast)
    {
        $broadcast->publish(new Message('topic', $this->argument('message')));
    }
}
