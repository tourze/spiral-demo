<?php
/**
 * {project-name}
 *
 * @author {author-name}
 */
declare(strict_types=1);

namespace App\Command;

use App\Job\UrlScrapeJob;
use Spiral\Console\Command;
use Spiral\Jobs\QueueInterface;
use Symfony\Component\Console\Input\InputArgument;

class UrlScrapeCommand extends Command
{
    protected const NAME = 'url-scrape';

    public const DESCRIPTION = '爬取网页';

    public const ARGUMENTS   = [
        ['url', InputArgument::REQUIRED, '要爬取的 URL'],
        ['depth', InputArgument::OPTIONAL, '要扫描的深度', 10],
    ];

    protected const OPTIONS = [];

    /**
     * Perform command
     */
    protected function perform(QueueInterface $queue): void
    {
        $queue->push(UrlScrapeJob::class, [
            'url'   => $this->argument('url'),
            'depth' => (int)$this->argument('depth')
        ]);

        $this->writeln("开始爬取!");
    }
}
