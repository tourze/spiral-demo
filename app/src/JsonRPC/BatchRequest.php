<?php

namespace App\JsonRPC;

class BatchRequest extends Request
{

    /**
     * @var array|Request[]
     */
    protected array $requests;

    /**
     * @return Request[]|array
     */
    public function getRequests(): array
    {
        return $this->requests;
    }

    /**
     * @param Request[]|array $requests
     */
    public function setRequests(array $requests): void
    {
        $this->requests = $requests;
    }
}
