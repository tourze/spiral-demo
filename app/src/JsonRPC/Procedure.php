<?php

namespace App\JsonRPC;

abstract class Procedure
{
    protected Request $requestContext;

    /**
     * @return Request
     */
    public function getRequestContext(): Request
    {
        return $this->requestContext;
    }

    /**
     * @param Request $requestContext
     */
    public function setRequestContext(Request $requestContext): void
    {
        $this->requestContext = $requestContext;
    }

    /**
     * 执行并返回结果
     *
     * @throws Error
     * @return array|object
     */
    abstract public function execute();
}
