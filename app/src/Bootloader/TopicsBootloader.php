<?php

namespace App\Bootloader;

use Psr\Http\Message\ServerRequestInterface;
use Spiral\Boot\Bootloader\Bootloader;
use Spiral\Bootloader\Http\WebsocketsBootloader;

class TopicsBootloader extends Bootloader
{
    public function boot(WebsocketsBootloader $ws)
    {
        // 参考 https://spiral.dev/docs/broadcast-websockets
        $ws->authorizeTopic(
            'topic',
            function (ServerRequestInterface $request) {
                // must return true for authorized topics
                return true;
            }
        );
    }
}
