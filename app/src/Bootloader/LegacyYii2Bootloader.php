<?php

namespace App\Bootloader;

use ReflectionClass;
use Spiral\Boot\Bootloader\Bootloader;
use Spiral\Core\Container;
use Spiral\Core\Exception\Container\ContainerException;

/**
 * 从实现原理来看，我们可以使用这个来兼容旧的Yii2写法
 * 参考文档 https://spiral.dev/docs/cookbook-injector
 */
class LegacyYii2Bootloader extends Bootloader implements Container\InjectorInterface, Container\SingletonInterface {

    public function createInjection(ReflectionClass $class, string $context = null)
    {
        // TODO: Implement createInjection() method.
    }
}
