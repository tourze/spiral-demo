<?php

namespace App\Bootloader;

use App\Middleware\ExceptionCatchMiddleware;
use Spiral\Boot\Bootloader\Bootloader;
use Spiral\Bootloader\Http\HttpBootloader;

class ExceptionCatchBootloader extends Bootloader
{

    public function boot(HttpBootloader $http)
    {
        $http->addMiddleware(ExceptionCatchMiddleware::class);
    }
}
