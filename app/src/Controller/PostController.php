<?php
/**
 * {project-name}
 *
 * @author {author-name}
 */
declare(strict_types=1);

namespace App\Controller;

use App\Database\Post;
use Psr\Http\Message\ResponseInterface;
use Spiral\Http\Exception\ClientException\NotFoundException;
use Spiral\Prototype\Traits\PrototypeTrait;
use Spiral\Router\Annotation\Route;

class PostController
{
    use PrototypeTrait;

    /**
     * @Route(route="/api/test/<id>", methods="GET")
     * @param string $id
     * @return array
     */
    public function test(string $id)
    {
        return [
            'status' => 200,
            'data'   => [
                'id' => $id
            ]
        ];
    }

    /**
     * @Route(route="/api/test1/<id>", methods="GET")
     * @param string $id
     * @return ResponseInterface
     */
    public function test1(string $id): ResponseInterface
    {
        return $this->response->json(
            [
                'data' => [
                    'id' => $id
                ]
            ],
            200
        );
    }

    /**
     * @Route(route="/api/post/<id:\d+>", name="post.get", methods="GET")
     * @param string $id
     * @return array
     */
    public function get(string $id)
    {
        /** @var Post $post */
        $post = $this->posts->findByPK($id);
        if ($post === null) {
            throw new NotFoundException("post not found");
        }

        return [
            'post' => [
                'id'      => $post->id,
                'author'  => [
                    'id'   => $post->author->id,
                    'name' => $post->author->name
                ],
                'title'   => $post->title,
                'content' => $post->content,
            ]
        ];
    }

    /**
     * @Route(route="/api/post1/<id:\d+>", name="post.get1", methods="GET")
     * @param Post $post
     * @return array
     */
    public function get1(Post $post)
    {
        return [
            'post' => [
                'id'      => $post->id,
                'author'  => [
                    'id'   => $post->author->id,
                    'name' => $post->author->name
                ],
                'title'   => $post->title,
                'content' => $post->content,
            ]
        ];
    }
}
