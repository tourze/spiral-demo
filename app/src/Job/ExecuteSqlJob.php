<?php

namespace App\Job;

use Spiral\Jobs\JobHandler;
use Spiral\Prototype\Traits\PrototypeTrait;

/**
 * 异步执行SQL
 */
class ExecuteSqlJob extends JobHandler
{
    use PrototypeTrait;

    public function invoke(string $sql, array $params = []): void
    {
        $this->db->execute($sql, $params);
    }
}
