package markdown

import (
    "github.com/russross/blackfriday/v2"
    "github.com/spiral/roadrunner/service/rpc"
)

const ID = "markdown"

// Service 是将注册到 app server 中的服务
type Service struct{}

func (s *Service) Init(rpc *rpc.Service) (bool, error) {
    if err := rpc.Register(ID, &rpcService{}); err != nil {
        return false, err
    }

    return true, nil
}

// rpcService 是通过 RPC 调用的服务
type rpcService struct{}

func (s *rpcService) Convert(input []byte, output *[]byte) error {
    *output = blackfriday.Run(input)
    return nil
}
