module github.com/spiral/framework

require (
	github.com/go-redis/redis v6.15.7+incompatible // indirect
	github.com/onsi/ginkgo v1.12.0 // indirect
	github.com/onsi/gomega v1.9.0 // indirect
	github.com/russross/blackfriday/v2 v2.1.0
	github.com/spiral/broadcast v0.0.0-20191206140608-766959683e74
	github.com/spiral/broadcast-ws v1.1.0
	github.com/spiral/jobs/v2 v2.2.0
	github.com/spiral/php-grpc v1.1.0
	github.com/spiral/roadrunner v1.8.3
)

go 1.15
